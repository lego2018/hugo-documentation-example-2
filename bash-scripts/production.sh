#!/usr/bin/env bash

while true; do
  read -p "Did you customize htaccess, robots, config, Rake and deploy? (y/n)" yn
  case $yn in
    [Yy]* ) nps clean.public;
            nps build.production.hugo;
            break;;

    [Nn]* ) clear;
            echo "---------------------------------------------------"
            echo "Okay, Please do that before going on..."
            echo "Customize .htaccess, humans, config, Rake and deploy"
            echo "and restart production-nomin build."
            echo "---------------------------------------------------"
            exit;;

      * ) echo "Please answer with Yes or No.";;
  esac
done
